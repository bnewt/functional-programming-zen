# Functional Programming and the path to zen.

## About this Presentation

This presentation will give an overview of whatfuncional programming is and how it can help you reduce friction, lower cognitive load, and ultimately write higher quality programs. Examples will be given primarily in F#, a mature, open source, strongly typed, functional-first, general purpose programming language that is fully interoperable with other .NET languages.

The aim of this presentation is not to be an in depth introduction to every aspect and feature of functional programming, but rather a survey of what makes it awesome and why you should seriously consider adopting functional programming techniques and/or languages. Previous functional programming experience is not required, but programming experience of some sort is assumed.

Topics covered will include: 
* Immutability and pure functions
* Higher-order functions 
* Function composition and pipelining 
* Partial function application 
* Pattern matching 
* Strongly typed functional programming 
* Type inference
* Unique and awesome F# features and why it is a great choice

Finally I will leave you with some recommended resources to start on your path to zen.

### Built with [FsReveal](https://github.com/fsprojects/FsReveal)