- title : Functional programming and the path to zen
- description : Overview of why functional programming leads to a more productive programming environment and why F# in particular is a great language for the job.
- author : Brian Newton
- theme : moon
- transition : default

***

### Functional programming and the path to zen

<br />
Brian Newton<br />
Solution Consultant<br />
LyncStream

Press the 'S' key to see speaker mode of the presentation with slide notes.

' Bachelors in CS from University of Nebraska at Omaha
' 
' Working w/ Microsoft technologies professionally for 8 years - primarily C# &amp; web development
' 
' Also like keep an eye on F# &amp; other functional languages
' 
' in free time I enjoy spending time with wife and our 2 dogs; working out, camping, traveling &amp; skiing

--- 

    [hide]
    
    // set up code

    open System

    type Person = { 
        Name: string
        DateOfBirth: DateTime }

    type Product = { 
        Sku: string;
        Description: string;
        Price: float }

    let pi = 3.14

    let getDob person = 
        person.DateOfBirth

    let getAge dob : int =
        (DateTime.Now - dob).Days / 365

    let personIsLessThan26 : Person -> bool =
        getPersonDOB >> personIsLessThan26

***

![LyncStream_Logo](images/lyncstream2017-color-lg.png)

Founded in 2012 with a simple mission: to provide cutting edge technology to businesses of every size and industry. Our mission is to build long term relationships with our clients and build technology solutions to accelerate growth, provide operation insight and automated business processes.

' quick shout out to my employer for giving me time off to give this presentation
' 
' great team of developers - always looking for more talent - chat with me after and see what we're about

*** 

### About this talk

* What is functional programming?
* How does it take you on the "Path to Zen"?
* What makes F# a particularly attractive language?

' Cover fundamental building blocks - focus on what makes FP awesome -  hopefully this encourages more investigation - recommended resources
' 
' As far as "Path to Zen" - I'll demonstrate features that  increase predictability, avoid common pitfalls &amp; remove repetition

---

### About the code samples

* All samples in F# unless specified
* Heavy use of type inference
* Simple examples, think bigger

' strongly typed, but doesn't look it
' 
' sweet spot between typed &amp; dynamic languages
' 
' demos are simple - take the concepts and expand

***

### What is Functional Programming?

Emphasis on modularity via composition of:

* Functions
* Data


' Boiled down: emphasis on modularity achieved via composition of functions and data - not just a math thing
' 
' Composition meaning consistent, predictable, language integrated ways to combine things together.
' 
' In addition to composition - a set of principles - make composition possible/easer

***

### How does it facilitate Composition?

* Immutability
* First Class Functions
* Composable Types
* Pattern Matching

' **Pause**
' 
' will cover all these & how they facilitate modularity and composition

***

### Immutability

* Limit & isolate *_shared_* mutable state
* Fully initialize objects to a valid state and freeze
* "Updates" create a new copy with desired changes

' FP seeks to limit &amp; isolate shared mutable state as much as possible
' 
' purpose of most software is to change state - mutable state in FP is fine, use it for the internal implementation details of functions - performance optimization
' 
' immutability here means objects fully initialized to valid state &amp; frozen once shared with the outside world
' 
' create updated copies of objects vs updating an objects properties

---

### Why Immutability?

* Makes explicit:
    * When & where updates are possible
    * Timing and order of data initialization
* Which:
    * Eliminates classes of errors
    * Allows looking at code in isolation
    * Simplifies concurrency

' Makes things explicit
' 
' Eliminates classes of errors - incomplete initialization, timing/ordering related, etc. - often pop up after initial development - after we've forgotten assumptions we had initially
' 
' Allows reasoning about code in isolation - as we'll see shortly
' 
' Most complexity in concurrency due to handling shared mutable state

---
    
    [hide]

    let foo(person:Person) = ()
    let bar(person:Person) = ()
    let baz(person:Person) = ()

---

### Mutable Example

    let mutableExample person =
        foo(person)
        bar(person)
        baz(person)

' Assume person might not be fully initialized and both foo &amp; bar can change person
' 
' Can I reorder foo() and bar() or do they need to be called in that order?
' 
' Do I need to call foo() and bar() before baz?

---
    
    [hide]

    type Person = { Name: string; DateOfBirth: DateTime }

    let foo(person:Person) = person.DateOfBirth
    let bar(person:Person) = person.Name
    let baz(x:DateTime,y:string) = ()

---

### Immutable Example

    let someFunction person =
        let a = foo(person)
        let b = bar(person)
        baz(a, b)

' assume foo() and bar() cannot change person
' 
' Can I reorder foo() and bar() &amp; do I need to call foo() and bar() before baz??
' 
' Adding constraints allows me to know more at a glance - great when coming back to code
' 
' NEXT: Candy bar discount example

---

### Immutability

    let candyBar = { 
        Sku = "ASDF1234"
        Description = "Candy Bar"
        Price = 2.5 }

    let discount amount product =
        { product with Price = product.Price - amount }
    
    let discountedCandyBar = discount 1.0 candyBar

    //    discountedCandyBar = { 
    //      Sku = "ASDF1234"
    //      Description = "Candy Bar"
    //      Price = 1.5 }

' *** Pause to go over the code ***
' 
' call the discount function to create a new updated "discountedCandyBar"
' 
' NEXT: We'll go over F#'s "with" syntax

---

### Immutability syntax

    let discount amount product =
        { product with Price = product.Price - amount }

#### vs

    let discount amount product = 
        { Sku = product.Sku
          Description = product.Description
          Price = product.Price - amount }

' Immutability adds extra pain when "updating" objects
' 
' FP languages let you lean on the compiler to alleviate it - less code => fewer bug opportunities
' 
' imagine if we had more than just 3 here - "with" syntax scales

---

### Summary: Why Immutability?

* Removes implicit assumptions
* Allows truly modular code
* Simplifies things on average

' if only one take-away - think about use of mutability - but really, have multiple take-aways ;)
' 
' biggest roadblock to producing maintainable software - this is why code becomes unmaintainable 
' 
' With mutability, have to be more defensive on when &amp; where objects change in the code base now &amp; after future changes - possibly by other developers
' 
' NEXT: first class functions

***

### First class functions

* Which enable:
    * Partial function application
    * Function composition
    * Transforming functions
    * Pipelining

' first class functions: functions can be passed as arguments, returned from functions & stored as variables
' 
' enables all of these - which we will cover next

---

### DEMO: Partial function application

    let add x y =
        x + y

    let add1 = add 1

    let result = add1 5
    // result is 6

' partial application: Call function w/ only some parameters -> returns function needing only remaining parameters
' 
' Allows "baking in" parameters
' 
' **Pause to go over what is happening in the code**
' 
' Not limited to numbers - functional dependency injection - can partially apply a Db connection for example
' 
' **Pause to let things sync in**
' 
' **NEXT UP** - What if I have a person and want to calculate how old they'll be next year?

---

### DEMO: Function Composition

    let jane = { 
        Name = "Jane"
        DateOfBirth = DateTime(1992,6,28) }

    let add1ToAge = getAge >> add1

    let add1ToPersonsAge person = 
        add1ToAge person.DateOfBirth

    let janesAgeNextYear = add1ToPersonsAge jane
    // result is 27

' composition: glue functions together - output of first -> input of next
' 
' **Pause to go over what is happening in the code - hover to display types**
' 
' built in and predictable - keeps code DRY - reused add1 function
' 
' NEXT: lets compare with manual composition

---

### Compare: Function Composition

    let add1ToAge = getAge >> add1

vs:

    let add1ToAge dob =
        let age = getAge dob
        add1 age

' Just one simple example, think about how much code in a large code base - aka opportunities for bugs - can be eliminated by composing using consistent, predictable, language integrated functionality
' 
' **NEXT UP** - What if I have a list of people and want to calculate the ages of everybody in the list next year?

---

### DEMO: Transforming functions

    let people = [ 
        { Name="Jane"; DateOfBirth=DateTime(1992,6,28) }
        { Name="John"; DateOfBirth=DateTime(1990,4,13) } ]

    let add1ToPeoplesAges = List.map add1ToPersonsAge

    let agesPlus1 = add1ToPeoplesAges people
    // agesPlus1 is [27; 25]

' transforming functions: operate on other functions - allow them work in new ways
' 
' transformed add1ToPersonsAge to operate on lists - didn't modify it at all
' 
' generic - didn't have to write our own "map" for list of int - F# std lib has many helpful transforming functions
' 
' Also DRY - keep building on previous usage of First Class Functions
' 
' **NEXT UP** - What if I want to get the names of the people who are younger than 26?

---

### DEMO: Pipelining
    
    people
    |> List.filter personIsLessThan26
    |> List.map (fun p -> person.Name)
    |> List.iter Console.WriteLine

    // prints "John" to the Console

' pipelining &amp; forward pipe operator
' 
' item above/left of operator gets passed as last argument of function on right of operator
' 
' Yan Cui pointed out in his blog post "Being visually honest with F#"
' ---- read left-to-right & top-to-bottom (more like english - less translation)
' 
' makes order of execution obvious at a glance - very nice for readability - keeps you in the flow when writing code as well

---

### Fist Class Functions: Putting it all together

    let add1ToAge = getDob >> getAge >> add1
    let lessThan26 = (<) 26

    people
    |> List.filter add1ToAge >> lessThan26
    |> List.map (fun p -> person.Name)
    |> List.iter Console.WriteLine

' note we partially applied 26 to the "less than" operator
'  
' NEXT: first class functions summary

---

### First Class Functions Summary

* Partial function application
* Function composition
* Transforming functions
* Pipelining

' techniques combine to create behavior composition utility belt - succinctly piece together small modules
' 
' no need to write glue functions yourself, FP makes those generic &amp; gets out of the way - your code becomes the focal point
' 
' allows easily and expressively building data flows - which is most of what programming is - coerce function argument/return types as well as number &amp; type of parameters
' 
' Terse, expressive code is easier to read, more declarative, and fits in your head - reducing cognitive load

***

### Composable Types

* Modeling in Functional Programming emphasizes
    * Composition
    * Separation of:
        * Data
        * State
        * Behavior

' Build types by combining other smaller types
' 
' Separate data, state &amp; behavior

---

### Records & Discriminated Unions

    type Phone = { 
        AreaCode : int
        Prefix : int
        LineNumber : int }

    type Email = EmailAddress of string

    type PreferredContactMethod =
        | ByEmail of Email
        | ByTextMessage of Phone
        | ByPushNotification of Phone

    type User = { 
        Name: string
        ContactMethod: PreferredContactMethod }

' Records allow modeling AND situations - where multiple pieces of data are expected together
' 
' DUs allow us to model OR situations - where type represents one of a set of options
' 
' Very terse &amp; very expressive for modeling - you can model whatever you need with the combination of AND/OR
' 
' Both pair nicely with Pattern matching as we'll see next

***

    [hide]
    let SendEmail:(Email -> string -> unit)

    let SendTextMessage:(Phone -> string -> unit) 

    let SendPushNotification:(Phone -> string -> unit)

---

### Pattern Matching

    let sendWelcomeMessage user msg =
        match user.ContactMethod with
        | ByEmail(addr) -> SendEmail addr msg
        | ByTextMessage(number) -> SendTextMessage number msg
        | ByPushNotification(number) -> SendPushNotification number msg

' code flow based on data shape/pattern - can pattern match on DU cases or one ore more record properties
' 
' *** Pause to explain code ***
' 
' NEXT: what if I want to add a special welcome message for certain users?

---

###  Pattern Matching(continued)
    let sendWelcomeMessage user msg =
        match user.ContactMethod with
        | ByEmail(addr) -> SendEmail addr msg
        | ByPushNotification(number) -> SendPushNotification number msg
        | ByTextMessage(number) ->
                match number with
                | { AreaCode=308 } 
                | { AreaCode=402 } 
                | { AreaCode=531 } -> 
                    sprintf "Hello %s! GBR!" user.Name |> SendTextMessage number
                | _ -> SendTextMessage number msg

' ByTextMessage - if we get NE area code - add a GBR to the welcome message
' 
' Notice the _ - it will match anything - can match on multiple properties at once
' 
' compiler complains if you don't cover all cases - allows you to be a lot more confident
' 
' NEXT: functional programming summary

*** 

### Functional Programming Summary

Emphasis on modularity via composition of:

* Functions
* Data

' Achieved with: immutability, first class functions, composable types &amp; pattern matching
' 
' NEXT: Why F#?

***

### Why F#?

* Multi-paradigm
* .Net/JS Interop
* Cross Platform & OSS
* Amazing tooling for a functional language

' functional first philosophy - also supports imperative & object programming paradigms
' 
' for .Net and JS devs, not an all or nothing switch - use it in places where it makes sense (.Net & Fable-JS interop)
' 
' emphasis on OSS/X-Plat even before rest of Microsoft
' 
' Visual Studio or VS Code w/ Ionide

---

### Why F#? - Good defaults

[Is your programming language unreasonable?](https://fsharpforfunandprofit.com/posts/is-your-language-unreasonable/)

* Variables should not be allowed to change their type.
* Objects containing the same values should be equal by default.
* Comparing objects of different types is a compile-time error.
* Objects must always be initialized to a valid state. Not doing so is a compile-time error.
* Once created, objects and collections must be immutable.
* No nulls allowed.
* Missing data or errors must be made explicit in the function signature.

' Scott Wlaschin of F# for fun and profit has a checklist on predictable code - Read It
' 
' all the defaults in F# - all eliminate classes of errors or make it easy to predict how code will behave in isolation
' 
' Solid defaults and paradigms - difference between requiring constant vigilance against architectural decay &amp; stress free maintainability
' 
' opt-out when necessary is a better policy - requires less knowledge than opt-in
' 
' NEXT: no nulls and explicit error return types

---

### No nulls allowed

    let printFirstNumGreaterThan5 nums =
        match nums |> List.tryFind ((>) 5) with
        | Some(num) -> sprintf "%s is greater than 5" num
        | None -> "no numbers greater than 5 found"
    
    [ 1; 2; 3] |> printFirstNumGreaterThan5 
    // prints "no numbers greater than 5 found"

    type Option<'a> =
    | Some of 'a
    | None

' Options used instead of null to signify missing data - I defined Option('a) following function to preserve intellisense
' 
' Tries to print the first item greater than 5
' 
' FirstOrDefault() is not equivalent to tryFind() when dealing with primitives - int list will return 0 if it doesn't find something or if 0 is in the list
' 
' You can use options as the type for record properties or DU data

---

### Errors must be made explicit

    type Result<'TSuccess,'TError> = 
    | Ok of 'TSuccess
    | Error of 'TError

    type PossibleFailures =
    | ServiceUnavailable
    | SquirrelBitTheLine

    type ExplicitReturnType =
        Result<Option<int>, PossibleFailures>

    // type: unit -> EplicitReturnType
    let explicitFunction() : ExplicitReturnType =
        if DateTime.Now.TimeOfDay < TimeSpan.FromHours(6) then
            Error(ServiceUnavailable)
        else
            match Random().Next(0, 100) with
            | num when num < 5 -> Error(SquirrelBitTheLine)
            | num when num < 10 -> Ok(None)
            | num -> Ok(Some(num))

' Result type allows us to encode success &amp; failure into the return type
' 
' *** Pause to go over code ***
' 
' Service Unavailable from midnight to 6AM
' 
' When operational - 5% return SquirrelBitTheLine Error - 5% successfully return Nothing - 90% successfully return the randomly generate number
' 
' First class functions - transforming functions in particular - help make functions work with Option &amp; Result types

***

### Extra features Demos

A few nice features from F#

* Spaces in function names
* Units of measure
* Type providers
* Transpile to JavaScript


' Here's a few bonus features from F#
' 
' There's actually a lot more - computation expressions for example - async/await syntax was inspired by those

---

### Spaces in function names

    let ``This is a function name. Can you believe it?!`` () = 
        Console.WriteLine("Hello F#!")

' quick but noteworthy one
' 
' really shines for tests - allows more meaningful test names, not constrained on underscores/punctuation

---

### Units of measure

* Metadata added to types to annotate units
* Compile time verification of unit correctness

![Units_Of_Measure](images/units-of-measure.png)

' motivated by 1999 loss of NASA‘s Climate Orbiter due to cross-team metric–imperial unit mixup
' 
' make it impossible to accidentally mix units or pass parameters of the same underlying type in the wrong order (ex: two floats, one representing acceleration and one representing distance)
' 
' Allows for high level of safety without much added effort
' 
' NEXT: some F# Interactive Outputs

---

### Units of measure

![Units_Of_Measure](images/units-of-measure.png)
![Units_Of_Measure_Output](images/units-of-measure-output.png)


' NEXT: transpile to JS

---

### Transpile to JavaScript

![Fable_Logo](images/fable-logo.png)

' F#-JS transpiler powered by Babel - designed to produce readable, idiomatic JS w/ nice JS library interop
' 
' Fable-Elmish w/ React is very popular - F# version of Model View Update architecture made popular by Elm

---

### Type providers

[FSharp.Data:](https://fsharp.github.io/FSharp.Data/)

![JSON Type Provider Example](images/JsonProvider.png)

' code generators on steroids - compiler calls into them at design time to generate type information(intellisense)
' 
' allows lazy loading - opens up possibility of generating types for only what you need from extremely large data sources
' 
' Type providers for Json, Csv, Xml, Sql, R, Powershell, WMI, the Registry, Regex &amp; quite a few other cool ones
' 
' Example image from https://fsharp.github.io/FSharp.Data/

---

### Things to explore

* Computation Expressions - Async, Seq, etc.
* Giraffe/Saturn
* SAFE Stack
* FAKE
* Paket
* Deedle
* FsTest & Unquote

' async/await syntax was inspired by async computation expressions
' 
' Giraffe/Saturn run on Asp.Net Core - SAFE stack is isomorphic F# on Giraffe/Saturn
' 
' Fake is F# Make build system - Paket sits on nuget, more similar to Yarn than nuget(lock file)
' 
' Deedle for Data Science - between type providers for all sort of input &amp; Python/R interop and F# interactive - F# needs a look for data scientists

***

### Resources that have helped on my path to zen

* Scott Wlaschin
    * Blog - [F# for fun and Profit](https://fsharpforfunandprofit.com/)
        * [Why use F#?](https://fsharpforfunandprofit.com/why-use-fsharp/)
        * [F# syntax in 60 seconds](https://fsharpforfunandprofit.com/posts/fsharp-in-60-seconds//)
        * [26 low-risk ways to use F# at work](https://fsharpforfunandprofit.com/posts/low-risk-ways-to-use-fsharp-at-work/)
    * Book - [Domain Modeling Made Functional](https://pragprog.com/book/swdddf/domain-modeling-made-functional)
* Rich Hickey
    * [Rich Hickey‘s Greatest Hits](https://changelog.com/posts/rich-hickeys-greatest-hits)
* John Skeet & Tomas Petricek
    * [Real-World Functional Programming](https://www.manning.com/books/real-world-functional-programming)
        * With Examples in F# and C# 

' Scott Wlaschin - best technical writers I have read - keeps it interesting / easy to read, avoids unnecessary jargon &amp; provides excellent examples
' 
' Rich Hickey is the creator of Clojure and a thought provoking presenter
' 
' Real World FP covers many functional &amp; F# language features with comparisons to C#

***

### Parting words

* Thanks for listening!

https://bnewt.gitlab.io/functional-programming-zen/

' only a sample of F# and FP - come chat if you're interested in learning more
' 
' FP takes a different mindset - type inference &amp; immutability awkward at first - completely normal, stick with it, they're worth it once they click
' 
' Finally, come to the dark side, we have cookies!

***

[https://bnewt.gitlab.io/functional-programming-zen](https://bnewt.gitlab.io/functional-programming-zen)

***

### References

[Yan Cui - "Being visually honest with F#"](https://hackernoon.com/being-visually-honest-with-f-d22208bda3c)

[Scott Wlaschin - "Is your programming language unreasonable?](https://fsharpforfunandprofit.com/posts/is-your-language-unreasonable)