

    [hide]

### What creates a productive programming environment?

* Low _unnecessary_ friction
* Rapid feedback loop
* Automated error detection
* Predictable code

' Remove ceremony, all friction should clearly pay for itself
' 
' The last three increase confidence and reduce anxiety


---

### Combined, these lead to:

Better results for less effort and stress

' TODO: more clear on what frees up effort/examples
' Frees up effort for other things
' 
' Stress != recipe for long term success

***

### Low unnecessary friction

* Remove as much ceremony as possible
* All friction should pay for itself
* Terse and expressive language

' Remove ceremony that slows getting the thoughts out of your head into clearly expressed code.
' 
' Even low friction items without payoff should still be removed. For example F# removed the requirement for the 'new' keyword on constructors - the compiler typically already knows what you mean. 
' 
' That might seem like a small thing, but it's extremely handy to not have to remove the keyword when switching from a constructor call to a static method call or vice versa.
' 
' However, things that provide long term acceleration are worth it.
'  
' Terse language - I'm not suggesting code golf or obfuscation contest style code, but terse languages that allow you to express higher level operations are more productive, otherwise we would all be programming in assembly or straight op-codes. There is a point of diminishing returns, but I we're not there yet with the most popular mainstream languages.
' 
' It's not about being cute - Terse languages increase the signal to noise ratio, which makes reading and understanding code easier once you are familiar with that language. It's kind of like removing the fine print from a contract.
' 
' Ultimately our job as developers often involves reading and understanding existing code quite a bit more than writing new code, wouldn't it be awesome if that were easier?
' 
' Being able to concisely express the same idea with less code is an advantage that adds up over time and pays dividends in both development time and maintenance.

---
### Low context switching

[This is why you shouldn't interrupt a programmer](images/this-is-why-you-shouldnt-interrupt-a-programmer.png)

* Reduce frequency of switching
* Reduce the cost of switching

' Show the why you shouldn't interrupt a programmer image
' 
' Less interruption increases the likelihood you will get in the zone and be productive. On the other hand, lots of interruptions cause mental fatigue .
' 
' It's important to reduce number of times you have break away from the problem you're trying to solve to "do the right thing" and perform menial tasks like creating classes and interfaces in a new file of a separate project.
' 
' To be clear, I'm not advocating against "doing the right thing", wouldn't it be better if "doing the right thing" didn't require so much ceremony?
' 
' Context switches WILL happen, reducing the cost of getting back in the zone when they do is also important.
' 
' One of the best ways to reduce the cost of switching is to reduce the size of the model you're working with and increase the locality of the code (smaller model, less spread out leads to faster re-absorption)

---
### Rapid feedback loop

* Decrease the time for feedback
* REPL (Read-Eval-Print Loop)

' Who here wants to wait for a CI build and deploy to debug or test out changes? That's an extreme example which I have actually had to endure, but feedback loop matters.
' 
' Gives reasonable confidence that code works, reducing unnecessary context switches back to broken code when gluing everything together.

---
### Javascript REPL

![JavaScript REPL](images/js-repl.png)

' Rapid feedback increases confidence and lowers mental burden of retaining trivia. If you're not sure on syntax or what result to expect, just try it out - no need to jump out of your IDE and look it up in the documentation.
' 
' The REPL reduces friction for experimenting. It has low requirements for state and context when running code and  allows for rapid prototyping and ad-hoc data exploration.
' 
' With the F# REPL - know as F# interactive - you are able to highlight blocks of code and send them to the REPL to be evaluated.
' 
' For many F# developers, the REPL is an integral part of their process. Most F# books and articles about getting started with F# begin with how to use and get comfortable with the REPL.
' 
' REPLs are very common in scripting languages and functional languages, however other languages are catching on. The C# REPL is called interactive.
' If you decide functional programming isn't for you, I would still encourage you to investigate if there is a REPL for your language.
' 
' Some of you are probably full stack developers used to setting breakpoints in the font end and possibly using the REPL to explore. "Imagine how much more productive you could be if you could have the ease of exploration in your server side language!" - @stachu

---

### Automated error detection

* Strong Typing
* Automated Testing

' Hopefully if you're using a strongly typed language, the compiler more than pays for its overhead by staying out of your way while still catching obvious mistakes.
' 
' Languages with type inference, automatic generalization, and sum types help accomplish this - we'll get into those later.
' 
' Type inference and automatic generalization mean you don't have to unnecessarily repeat yourself, something that adds friction to refactoring. They make things look and feel like a dynamic language, but retain all the benefits of a strongly typed one.
' 
' Automated tests - which are separate from the programming paradigm you're using - can also help you to ensure that your code is and remains working as you expect.
' 
' Automated testing and strong typing both help reduce anxiety around correctness and increase confidence when refactoring, while also allowing you to produce quality code faster and reduce context switches back to broken code.

---

### Code should be predictable

* You should be able to predict what code will do by looking at it
    * Prediction correct after changing _other parts of the codebase_

' It's really hard to confidently work with _anything_ that is unpredictable
' 
' If code is unpredictable, at least one two things is true: it's too complex or you don't yet understand it.
' 
' You shouldn't have to worry that code will work today but break tomorrow by changing other parts of the code - spooky action at a distance sucks.
' 
' "Have you ever had a case where you've passed a variable into a method, and once it's done, it's somehow different? F# prevents this confusion and therefore prevents bugs!" - @stachu
' 
' When it comes down to it, this is really about having self contained modular code.

***

### Why functional programming?
' Paul Graham, The founder of the first web startup viaweb - now known as Yahoo! Store - and later co-founder of Y-Combinator, wrote an essay called 'Beating the averages' outlining how functional programming was a competitive advantage for them when building Viaweb. - http://www.paulgraham.com/avg.html
' 
' Functional programming typically allows you to solve harder problems more effectively with fewer programmers - Many examples Jet.com, WhatsApp, Viaweb(the first web startup), and more.
' 
' Pause to emphasize success stories such as Jet.com. Also mention some of the happiest and best paid developers on the Stackoverflow developer survey.

***

### Types & DUs Notes

' Notice how compact the type definitions were for this. It's hard to show a conference demo that illustrates code being closer together rather than spread out, but as you can imagine the type definitions for Shape might live in 4 separate spread out files in another language.
' 
' As a side note on how F# types come with batters fully charged - by default records and discriminated unions are immutable, implement GetHashCode() and are equal if they contain all the same values.

***

### Why F#?

' TODO: Tie back in with productive programming environment. Good type of strongly typed, terse language with data modeling that encourages smaller models that are located closer together - Related data structures and functions are often grouped closely together, even in the same file.